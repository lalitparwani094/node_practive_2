const express = require('express');

var app = express();

app.get('/', (req, res) => {
    res.send('Hello world!');
});

/*app.get('/', (req, res) => {
    res.status(404).send({
        error: 'Page not found.'
    });
});

app.get('/', (req, res) => {
    res.status(404).send({
        error: 'Page not found.',
        name: 'abc def'
    });
});*/

app.get('/users', (req, res) => {
    res.send([{
        name: 'abc',
        age: 27
    }, {
        name: 'def',
        age: 25
    }, {
        name: 'xyz',
        age: 26
    }]);
});

app.listen(3000);

module.exports.app = app;