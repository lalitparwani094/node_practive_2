const expect = require('expect');
const utils = require('./utils');

describe('Utils', () => {
    describe('#add', () => {
        it('should add two numbers', () => {
            var res = utils.add(33, 11);
        
            // if(res != 44) {
            //     throw new Error(`Expected 44, but got ${res}.`)
            // }
        
            //expect(res).toBe(44);
            expect(res).toBe(44).toBeA('number');
        });
    });

    it('should square a number', () => {
        var res = utils.square(3);
    
        // if(res != 9) {
        //     throw new Error(`Expected 9, but got ${res}.`)
        // }
        //expect(res).toBe(9);
        expect(res).toBe(9).toBeA('number');
    });
    
    it('should expect some values', () => {
        //expect(12).toNotBe(12);
        //expect(12).toNotBe(11)
        //expect({name: 'abc'}).toBe({name: 'abc'});
        //expect({name: 'abc'}).toNotEqual({name: 'abc'});
        //expect({name: 'abc'}).toEqual({name: 'abc'});
        //expect([2,3,4]).toInclude(1);
        //expect([2,3,4]).toInclude(2);
        //expect([2,3,4]).toExclude(2);
        //expect([2,3,4]).toExclude(1);
        expect({
            name: 'abc',
            age: 25,
            location: 'def'
        }).toExclude({
            age: 23
        });
    });
    
    it('should set firstName and lastName', () => {
        var user = {location: 'def', age: 25};
        var res = utils.setName(user, 'abc xyz');
    
        expect(res).toInclude({
            firstName: 'abc',
            lastName: 'xyz'
        });
    });
    
    it('should async add two numbers', (done) => {
        utils.asyncAdd(4, 3, (sum) => {
            expect(sum).toBe(7).toBeA('number');
            done();
        });
    });
    
    it('should async square a number', (done) => {
        utils.asyncSquare(5, (sum) => {
            expect(sum).toBe(25).toBeA('number');
            done();
        });
    });
});