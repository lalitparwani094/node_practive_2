const yargs = require('yargs');

const geocode = require('./geocode.js');
const weather = require('./weather.js');

const argv = yargs
    .options({
        a: {
            demand: true,
            alias: 'address',
            describe: 'Address to fetch weather for',
            string: true
        }
    })
    .help()
    .alias('help', 'h')
    .argv;

geocode.geocodeAddress(argv.address).then((results) => {
    console.log(errorMessage);
}, (errorMessage) => {
    //console.log(results.address);
    //weather.getWeather(results.latitude, results.longitude, (errorMessage, weatherResults) => {
    weather.getWeather(37.8267, -122.4233).then((weatherResults) => {
        console.log(`It's currently ${weatherResults.temperature}. It feels like ${weatherResults.apparentTemperature}`);
    }, (errorMessage) => {
        console.log(errorMessage);
    });
});