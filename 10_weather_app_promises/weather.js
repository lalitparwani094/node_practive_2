const request = require('request');

var getWeather = (lat, lng) => {
    return new Promise((resolve, reject) => {
        var apiKey = '8cc8144894296349ee55a38fc43ab9ce';

        console.log(`latitude:${lat}`);
        console.log(`longitude:${lng}`);
        request({
            url: `https://api.darksky.net/forecast/${apiKey}/${lat},${lng}`,
            json: true
        }, (error, response, body) => {
            if (error) {
                reject('Unable to connect to Forecast.io server.');
            } else if (response.statusCode === 400) {
                reject('Unable to fetch weather.');
            } else if (response.statusCode === 200) {
                resolve({
                    temperature: body.currently.temperature,
                    apparentTemperature: body.currently.apparentTemperature
                });
            }
        });
    });
};

module.exports.getWeather = getWeather;