const request = require('request');

var getWeather = (lat, lng, callback) => {
    var apiKey = '8cc8144894296349ee55a38fc43ab9ce';

    console.log(`latitude:${lat}`);
    console.log(`longitude:${lng}`);
    request({
        url: `https://api.darksky.net/forecast/${apiKey}/${lat},${lng}`,
        json: true
    }, (error, response, body) => {
        if (error) {
            callback('Unable to connect to Forecast.io server.');
        } else if (response.statusCode === 400) {
            callback('Unable to fetch weather.');
        } else if (response.statusCode === 200) {
            callback(undefined, {
                temperature: body.currently.temperature,
                apparentTemperature: body.currently.apparentTemperature
            });
        } else {
            console.log('inside weather2');
        }
    });
};

module.exports.getWeather = getWeather;