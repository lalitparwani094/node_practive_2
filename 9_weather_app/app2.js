const request = require('request');
const yargs = require('yargs');

const argv = yargs
    .options({
        a: {
            demand: true,
            alias: 'address',
            describe: 'Address to fetch weather for',
            string: true
        }
    })
    .help()
    .alias('help', 'h')
    .argv;

request({
    url: `https://api.darksky.net/forecast/8cc8144894296349ee55a38fc43ab9ce/37.8267,-122.4233`,
    json: true
}, (error, response, body) => {
    if (error) {
        console.log('Unable to connect to Forecast.io server.');
    } else if (response.statusCode === 400) {
        console.log('Unable to fetch weather.');
    } else if (response.statusCode === 200) {
        console.log(body.currently.temperature);
    }
});