const request = require('request');

var geocodeAddress = (address, callback) => {
    var encodedAddress = encodeURIComponent(address);
    var apiKey = 'AIzaSyApTQDLgPIwCrwhXJ30GRkF9sW2eDpWK1g';

    request({
        url: `https://maps.googleapis.com/maps/api/geocode/json?key=${apiKey}&address=${encodedAddress}`,
        json: true
    }, (error, response, body) => {
        if (error) {
            callback('Unable to connect to Google servers.');
        } else if (body.status === 'ZERO_RESULTS') {
            callback('Unable to find that address.');
        } else if (body.status === 'REQUEST_DENIED') {
            callback('Request is denied.');
        } else if (body.status === 'OK') {
            callback(undefined, {
                address: body.results[0].formatted_address,
                latitude: body.results[0].geometry.location.lat,
                longitude: body.results[0].geometry.location.lng
            });
        } else {
            var abc = 'abc';
            callback(undefined, {
                abc
            });
        }
    });
};

module.exports.geocodeAddress = geocodeAddress;