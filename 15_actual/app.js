const jsonDiff = require('json-diff');
const fs = require('fs');

console.log(jsonDiff.diffString({ foo: 'bar' }, { foo: 'baz' }));

// Output:
//  {
// -  foo: "bar"
// +  foo: "baz"
//  }

console.log(jsonDiff.diff({ foo: 'bar' }, { foo: 'baz', a:'abc' }));

//console.log(jsonDiff.diff([{x:1},2,3], [1,4,3,2,{x:9}]));

// Output:
// { foo: { __old: 'bar', __new: 'baz' } }

let s = {
    "Mitigation": {
      "ContractorIdentification": "300",
      "ContractorSelection": "300",
      "CustomerSurveyAndPolicyHolderSignOff": "7200",
      "EstimateApproved": "86400",
      "EstimateReview": "86400",
      "EstimateUpload": "86400",
      "FirstVisit": "14400",
      "MitigationWorkComplete": "86400",
      "MitigationWorkInProgress": "3600"
    },
    "MitigationRestoration": {
      "ContractorIdentification": "300",
      "ContractorSelection": "300",
      "EstimateApproved": "3600",
      "EstimateReview": "7200",
      "EstimateUpload": "28800",
      "FirstVisit": "14400",
      "JobSchedule": "28800",
      "MitigationWorkComplete": "10800",
      "MitigationWorkInProgress": "10800"
    },
    "Restoration": {
      "ContractorIdentification": "43200",
      "ContractorSelection": "21600",
      "EstimateApproved": "86400",
      "EstimateReview": "43200",
      "EstimateUpload": "86400",
      "FirstVisit": "172800",
      "JobSchedule": "172800"
    }
  };

  let s2 = {
    "Mitigation": {
      "ContractorIdentification": "300",
      "ContractorSelection": "300",
      "CustomerSurveyAndPolicyHolderSignOff": "7200",
      "EstimateApproved": "86400",
      "EstimateReview": "86400",
      "EstimateUpload": "86400",
      "FirstVisit": "14400",
      "MitigationWorkComplete": "86400",
      "MitigationWorkInProgress": "3600"
    },
    "MitigationRestoration": {
      "ContractorIdentification": "300",
      "ContractorSelection": "300",
      "EstimateApproved": "3600",
      "EstimateReview": "7200",
      "EstimateUpload": "28800",
      "FirstVisit": "14400",
      "JobSchedule": "28800",
      "MitigationWorkComplete": "10800",
      "MitigationWorkInProgress": "10800"
    },
    "Restoration": {
      "ContractorIdentification": "43200",
      "ContractorSelection": "21600",
      "EstimateApproved": "86400",
      "EstimateUpload": "86400",
      "EstimateReview": "43200",
      "FirstVisit": "172801",
      "JobSchedule": "172800",
      "JobSchedule2": "172800"
    }
  };


let diff = jsonDiff.diff(s, s2);

//console.log(diff);

// for(let s1 in s) {
//     for(let status in s['Mitigation']) {
//         console.log(`'abc',${s1}, ${status}, ${s['Mitigation']['ContractorIdentification']}`);
//     }
// }

let locations = [
    {
      "id": 0,
      "itemName": "Alabama"
    }
  ];

// for(let i = 0; i < locations.length; i++) {
//     console.log(locations[i].itemName);
// }

locations.forEach((locations) => {
    //console.log(locations.itemName);
});


let abc = {
    "completedStatus": {
      "endDate": 1566545056.999,
      "stage": "contractorDeployment",
      "startDate": 1566545020,
      "status": "contractorSelection"
    },
    "currentStatus": {
      "slaAmber": 1566717857,
      "slaAmberAdminNotified": 1,
      "slaAmberAdminReminded": 1,
      "slaAmberNotifiedContractor": true,
      "slaGreen": 1566674657,
      "slaGreenAdminNotified": 1,
      "slaGreenNotifiedContractor": true,
      "stage": "contractorDeployment",
      "startDate": 1566545057,
      "status": "firstSiteVisit"
    },
    "whgStages": [
      {
        "name": "claimInitiation",
        "statuses": [
          {
            "displayName": "First Notice Of Loss",
            "endDate": 1566544966,
            "name": "firstNoticeOfLoss",
            "startDate": 1566544966,
            "value": 1
          },
          {
            "displayName": "First Contact",
            "endDate": 1566544970,
            "name": "firstContact",
            "startDate": 1566544966,
            "value": 1
          },
          {
            "displayName": "Contractor Initiation",
            "endDate": 1566544984,
            "name": "contractorInitiation",
            "startDate": 1566544970,
            "value": 1
          }
        ]
      },
      {
        "name": "contractorDeployment",
        "statuses": [
          {
            "displayName": "Contractor Identification",
            "endDate": 1566545019.606,
            "name": "contractorIdentification",
            "sla": "43200",
            "slaAmber": 1566588184,
            "slaGreen": 1566577384,
            "startDate": 1566544984,
            "value": 1
          },
          {
            "contractor": {
              "ContractorId": "us-west-2:b32d6645-8c98-49dc-b7c8-bec6f964592f",
              "FirstName": "James",
              "LastName": "Hill"
            },
            "displayName": "Contractor Selection",
            "endDate": 1566545056.999,
            "name": "contractorSelection",
            "sla": "21600",
            "slaAmber": 1566566620,
            "slaGreen": 1566561220,
            "startDate": 1566545020,
            "value": 1
          },
          {
            "displayName": "First Site Visit",
            "name": "firstSiteVisit",
            "sla": "172800",
            "slaAmber": 1566717857,
            "slaGreen": 1566674657,
            "startDate": 1566545057,
            "value": 0
          },
          {
            "displayName": "Estimate Upload",
            "name": "estimateUpload",
            "sla": "86400",
            "value": 0
          },
          {
            "displayName": "Estimate Review",
            "name": "estimateReview",
            "sla": "43200",
            "value": 0
          },
          {
            "displayName": "Estimate Approval",
            "name": "estimateApproval",
            "sla": "86400",
            "value": 0
          },
          {
            "displayName": "Job Schedule",
            "name": "jobSchedule",
            "sla": "172800",
            "value": 0
          }
        ]
      },
      {
        "name": "jobInProgress",
        "statuses": [
          {
            "displayName": "Job Start",
            "name": "jobStart",
            "value": 0
          },
          {
            "displayName": "Materials Order",
            "name": "materialsOrder",
            "value": 0
          },
          {
            "displayName": "Materials Deliver",
            "name": "materialsDeliver",
            "value": 0
          },
          {
            "displayName": "Materials Installation",
            "name": "materialsInstallation",
            "value": 0
          },
          {
            "displayName": "Job Completion",
            "name": "jobCompletion",
            "value": 0
          },
          {
            "displayName": "Customer Survey And PolicyHolder SignOff",
            "name": "customerSurveyAndPolicyHolderSignOff",
            "value": 0
          }
        ]
      }
    ]
  };


    //  abc.whgStages.forEach((stag) => {
    //     console.log('stagename',stag['name']);
    //     stag['statuses'].forEach((status) => {
    //         console.log(status['displayName1']);
    //     });
    //  });

let allSpec = [
    {
      "Checked": [
        "true"
      ],
      "label": "Contractor - Commercial",
      "subCategories": [

      ]
    },
    {
      "Checked": [
        "true"
      ],
      "label": "Contractor - Construction Expert",
      "subCategories": [

      ]
    },
    {
      "Checked": [
        "true"
      ],
      "label": "Contractor - Contractor Referral",
      "subCategories": [

      ]
    },
    {
      "Checked": [
        "true"
      ],
      "label": "Contractor - GC",
      "subCategories": [

      ]
    },
    {
      "Checked": [
        "true"
      ],
      "label": "Contractor - Pool Enclosures",
      "subCategories": [

      ]
    },
    {
      "Checked": [
        "true"
      ],
      "label": "Expert Services - Abatement - Lead",
      "subCategories": [

      ]
    },
    {
      "Checked": [
        "true"
      ],
      "label": "Expert Services - Cleaning - Carpet",
      "subCategories": [

      ]
    },
    {
      "Checked": [
        "true"
      ],
      "label": "Expert Services - Cleaning - Structural Smoke / Fire",
      "subCategories": [

      ]
    },
    {
      "Checked": [
        "true"
      ],
      "label": "Expert Services - Cleaning - Textile",
      "subCategories": [

      ]
    },
    {
      "Checked": [
        "true"
      ],
      "label": "Expert Services - Testing Services",
      "subCategories": [

      ]
    }
  ];

//   allSpec.forEach((spec) => {
//         console.log(spec['label']);
//   });

  let location = {
    "New York": {
      "LocationStatus": "Approved",
      "PostalCodePA": "380051",
      "StatePA": "New York"
    }
  };

  // for(let loc in location) {
  //   console.log(loc);
  // }

fs.appendFile('abc.txt','Hello\n', function (err) {
  if (err) throw err;
  console.log('Saved!');
})

console.log(Math.floor(Math.random() * 1000));

console.log('code',Math.random().toString().substring(2));

let text = '';
let possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

for (let i = 0; i < 5; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

console.log('text',text);


let date1 = {

};

let date2 = {};

let cont = {
  "CompanyName": "Turner Co.",
  "ContractorId": "us-west-2:b32d6645-8c98-49dc-b7c8-bec6f964592f",
  "FirstName": "James",
  "LastName": "Hill",
  "ParentContractorId": "us-west-2:02d4cd73-ba06-4a4d-a754-c854329b7597"
};

let abcd = {
  "attributes": {
    "City": "NY",
    "Country": "UnitedStates",
    "Line1": "A - 23-8-444",
    "Line2": null,
    "State": "NewYork",
    "StatePA": "New York",
    "ZipCode": "00501"
  }
};

console.log('name ',abcd.attributes.City);

console.log('CompanyName ',cont.CompanyName);

console.log('abc', parseInt(date1) || 0);

let invoices = [
  {
    "qbCustomerId": "58",
    "qbInvoiceAmount": 103.52,
    "qbInvoiceDate": 1564146982000,
    "qbInvoiceNumber": "1086",
    "qbInvoiceStatus": "EmailSent"
  }
];

console.log(invoices.forEach((invoice) => {
  console.log('invoice customer id ', invoice.qbCustomerId);
}));

let currentTimeStamp = Math.round((new Date()).getTime() / 1000);
console.log('currentTimeStamp', currentTimeStamp);

let x = 123.234545;
console.log('value', +x.toFixed(2));


let a1 = {
  "FailedLoginAttempts": {
    "N": "0"
  },
  "VerificationStatus": {
    "S": "Active"
  },
  "FoundedYear": {
    "S": "2000"
  },
  "OTPExpires": {
    "N": "1563887252"
  },
  "Email": {
    "S": "sagar.panchasara@mailinator.com"
  },
  "CompanyID": {
    "S": "422"
  },
  "StatePA": {
    "S": "New York"
  },
  "OTP": {
    "S": "5071"
  },
  "ContractorIdEncr": {
    "S": "2305da72dcb55ee8c8155297a68a8772"
  },
  "MobileVerification": {
    "N": "1"
  },
  "TypeOfSpecialities": {
    "L": [{
        "M": {
          "Checked": {
            "L": [{
                "S": "true"
              }
            ]
          },
          "label": {
            "S": "Contractor - Construction Expert"
          },
          "subCategories": {
            "L": []
          }
        }
      }, {
        "M": {
          "Checked": {
            "L": [{
                "S": "true"
              }
            ]
          },
          "label": {
            "S": "Contractor - GC"
          },
          "subCategories": {
            "L": []
          }
        }
      }, {
        "M": {
          "Checked": {
            "L": [{
                "S": "true"
              }
            ]
          },
          "label": {
            "S": "Contractor - Plumbing Referrals"
          },
          "subCategories": {
            "L": []
          }
        }
      }, {
        "M": {
          "Checked": {
            "L": [{
                "S": "true"
              }
            ]
          },
          "label": {
            "S": "Contractor - Restoration - Fire"
          },
          "subCategories": {
            "L": []
          }
        }
      }, {
        "M": {
          "Checked": {
            "L": [{
                "S": "true"
              }
            ]
          },
          "label": {
            "S": "Contractor - Restoration - Hail Damage"
          },
          "subCategories": {
            "L": []
          }
        }
      }, {
        "M": {
          "Checked": {
            "L": [{
                "S": "true"
              }
            ]
          },
          "label": {
            "S": "Contractor - Restoration - Mold Damage"
          },
          "subCategories": {
            "L": []
          }
        }
      }, {
        "M": {
          "Checked": {
            "L": [{
                "S": "true"
              }
            ]
          },
          "label": {
            "S": "Contractor - Restoration - Smoke"
          },
          "subCategories": {
            "L": []
          }
        }
      }, {
        "M": {
          "Checked": {
            "L": [{
                "S": "true"
              }
            ]
          },
          "label": {
            "S": "Contractor - Restoration - Terrorism Damage"
          },
          "subCategories": {
            "L": []
          }
        }
      }, {
        "M": {
          "Checked": {
            "L": [{
                "S": "true"
              }
            ]
          },
          "label": {
            "S": "Contractor - Restoration - Vandalism Damage"
          },
          "subCategories": {
            "L": []
          }
        }
      }, {
        "M": {
          "Checked": {
            "L": [{
                "S": "true"
              }
            ]
          },
          "label": {
            "S": "Contractor - Restoration - Water Damage"
          },
          "subCategories": {
            "L": []
          }
        }
      }, {
        "M": {
          "Checked": {
            "L": [{
                "S": "true"
              }
            ]
          },
          "label": {
            "S": "Contractor - Single Trade - Drywall"
          },
          "subCategories": {
            "L": []
          }
        }
      }
    ]
  },
  "employeeNo": {
    "S": "66"
  },
  "CityMA": {
    "S": "Hostville"
  },
  "StartDate": {
    "N": "1567759364"
  },
  "CompanyName": {
    "S": "Load Test - Granite Construction Office"
  },
  "ClaimIdsCurrentlyWorkingOn": {
    "L": []
  },
  "CompanyWebsite": {
    "S": "www.granite.com"
  },
  "IsParentOnboarded": {
    "N": "1"
  },
  "PasswordReset": {
    "N": "1"
  },
  "ProfileGorillaParentOrganizationGUID": {
    "S": "f90774c7-2ddd-4246-9c38-9a8075c4c358"
  },
  "SignUpDate": {
    "N": "1567759365"
  },
  "StreetLine2PA": {
    "S": "4"
  },
  "EmailVerification": {
    "N": "1"
  },
  "ServiceTerritory": {
    "L": [{
        "S": "00501"
      }, {
        "S": "00605"
      }, {
        "S": "00610"
      }, {
        "S": "00613"
      }, {
        "S": "00617"
      }, {
        "S": "00623"
      }, {
        "S": "00627"
      }, {
        "S": "00631"
      }
    ]
  },
  "Locations": {
    "M": {
      "New York": {
        "M": {
          "StatePA": {
            "S": "New York"
          },
          "PostalCodePA": {
            "S": "00501"
          },
          "LocationStatus": {
            "S": "Approved"
          }
        }
      }
    }
  },
  "CognitoReferenceId": {
    "S": "42595544046734671567759365"
  },
  "CompanyEmail": {
    "S": "graniteoffice@dispostable.com"
  },
  "PostalCodePA": {
    "S": "00501"
  },
  "CompanyLogo": {
    "S": "6af8307c2460f2d208ad254f04be4b0d/hUnUG4bmQHegriN7Ke1s26Rz.png"
  },
  "RenewalDate": {
    "N": "1572480000"
  },
  "PGCompanyLogoDocumentId": {
    "S": "0225a429-542b-43bc-8be3-fcec6cb76ee1"
  },
  "LastProspectContractorsNotified": {
    "N": "1567759364"
  },
  "FirstLoginAttempted": {
    "N": "1"
  },
  "StreetLine2MA": {
    "S": "3"
  },
  "LastName": {
    "S": "LT_Kell"
  },
  "CompanyMobileNumber": {
    "S": "121-324-2425"
  },
  "NumberofEmployees": {
    "N": "10"
  },
  "ContractorsCount": {
    "N": "1"
  },
  "EmailVerificationExpires": {
    "S": "8/7/2019"
  },
  "PostalCodeMA": {
    "S": "00501"
  },
  "ContractorId": {
    "S": "us-west-2:6636ea04-1716-483a-9767-01974f91c068"
  },
  "StatePA_Abbr": {
    "S": "NY"
  },
  "DeviceTokens": {
    "L": [{
        "M": {
          "platform": {
            "S": "Web"
          },
          "deviceToken": {
            "S": "ejyUSLpw3Lk:APA91bEaDW6dzpFlJEyVIjTJ9x-y2HCY9D9gdJ0zHEWcX4Ojb81VhEB1jiyCDU1S5JqOE-5xwNDuMxEAtVroj1_xTaTFjfYMfdOgWA96PS2tbi6bhcf4mphwPDGTWsqQh1ToRneqsV1k"
          }
        }
      }
    ]
  },
  "ProfileImage": {
    "S": "2305da72dcb55ee8c8155297a68a8772/ede6febfd4f22e9d3c078c1fe8dbc8e2.png"
  },
  "ContractorType": {
    "S": "Admin"
  },
  "StreetLine1PA": {
    "S": "45-D West Down"
  },
  "Password": {
    "S": "f925916e2754e5e03f75dd58a5733251"
  },
  "TypeOfCompany": {
    "S": "Limited Liability Company LLC"
  },
  "StateMA_Abbr": {
    "S": "NY"
  },
  "StateMA": {
    "S": "New York"
  },
  "Submitted": {
    "N": "0"
  },
  "WorkExperience": {
    "N": "19"
  },
  "ContractorStatus": {
    "N": "1"
  },
  "ProfileGorillaId": {
    "S": "test_data"
  },
  "FirstName": {
    "S": "Test"
  },
  "CityPA": {
    "S": "Hostville"
  },
  "IsCurrentlyWorkingOnClaim": {
    "N": "0"
  },
  "LastWorkedOnClaimTimestamp": {
    "N": "1567759365"
  },
  "MobileNumber": {
    "S": "+11234567890"
  },
  "EmailVerificationCode": {
    "S": "2940434392293323"
  },
  "StreetLine1MA": {
    "S": "3/D Blue yard"
  },
  "ProfileGorillaUserId": {
    "S": "LT_updt4"
  },
  "StatusUpdateDate": {
    "N": "1567759364"
  }
};

let a2 = {
  "FailedLoginAttempts": {
    "N": "0"
  },
  "VerificationStatus": {
    "S": "Active"
  },
  "FoundedYear": {
    "S": "2000"
  },
  "OTPExpires": {
    "N": "1563887252"
  },
  "Email": {
    "S": "sagar.panchasara@mailinator.com"
  },
  "CompanyID": {
    "S": "422"
  },
  "StatePA": {
    "S": "New York"
  },
  "OTP": {
    "S": "5071"
  },
  "ContractorIdEncr": {
    "S": "2305da72dcb55ee8c8155297a68a8772"
  },
  "MobileVerification": {
    "N": "1"
  },
  "TypeOfSpecialities": {
    "L": [{
        "M": {
          "Checked": {
            "L": [{
                "S": "true"
              }
            ]
          },
          "label": {
            "S": "Contractor - Construction Expert"
          },
          "subCategories": {
            "L": []
          }
        }
      }, {
        "M": {
          "Checked": {
            "L": [{
                "S": "true"
              }
            ]
          },
          "label": {
            "S": "Contractor - GC"
          },
          "subCategories": {
            "L": []
          }
        }
      }, {
        "M": {
          "Checked": {
            "L": [{
                "S": "true"
              }
            ]
          },
          "label": {
            "S": "Contractor - Plumbing Referrals"
          },
          "subCategories": {
            "L": []
          }
        }
      }, {
        "M": {
          "Checked": {
            "L": [{
                "S": "true"
              }
            ]
          },
          "label": {
            "S": "Contractor - Restoration - Fire"
          },
          "subCategories": {
            "L": []
          }
        }
      }, {
        "M": {
          "Checked": {
            "L": [{
                "S": "true"
              }
            ]
          },
          "label": {
            "S": "Contractor - Restoration - Hail Damage"
          },
          "subCategories": {
            "L": []
          }
        }
      }, {
        "M": {
          "Checked": {
            "L": [{
                "S": "true"
              }
            ]
          },
          "label": {
            "S": "Contractor - Restoration - Mold Damage"
          },
          "subCategories": {
            "L": []
          }
        }
      }, {
        "M": {
          "Checked": {
            "L": [{
                "S": "true"
              }
            ]
          },
          "label": {
            "S": "Contractor - Restoration - Smoke"
          },
          "subCategories": {
            "L": []
          }
        }
      }, {
        "M": {
          "Checked": {
            "L": [{
                "S": "true"
              }
            ]
          },
          "label": {
            "S": "Contractor - Restoration - Terrorism Damage"
          },
          "subCategories": {
            "L": []
          }
        }
      }, {
        "M": {
          "Checked": {
            "L": [{
                "S": "true"
              }
            ]
          },
          "label": {
            "S": "Contractor - Restoration - Vandalism Damage"
          },
          "subCategories": {
            "L": []
          }
        }
      }, {
        "M": {
          "Checked": {
            "L": [{
                "S": "true"
              }
            ]
          },
          "label": {
            "S": "Contractor - Restoration - Water Damage"
          },
          "subCategories": {
            "L": []
          }
        }
      }, {
        "M": {
          "Checked": {
            "L": [{
                "S": "true"
              }
            ]
          },
          "label": {
            "S": "Contractor - Single Trade - Drywall"
          },
          "subCategories": {
            "L": []
          }
        }
      }
    ]
  },
  "employeeNo": {
    "S": "66"
  },
  "CityMA": {
    "S": "Hostville"
  },
  "StartDate": {
    "N": "1567759364"
  },
  "CompanyName": {
    "S": "Load Test - Granite Construction Office"
  },
  "ClaimIdsCurrentlyWorkingOn": {
    "L": []
  },
  "CompanyWebsite": {
    "S": "www.granite.com"
  },
  "IsParentOnboarded": {
    "N": "1"
  },
  "PasswordReset": {
    "N": "1"
  },
  "ProfileGorillaParentOrganizationGUID": {
    "S": "f90774c7-2ddd-4246-9c38-9a8075c4c358"
  },
  "SignUpDate": {
    "N": "1567759365"
  },
  "StreetLine2PA": {
    "S": "4"
  },
  "EmailVerification": {
    "N": "1"
  },
  "ServiceTerritory": {
    "L": [{
        "S": "00501"
      }, {
        "S": "00605"
      }, {
        "S": "00610"
      }, {
        "S": "00613"
      }, {
        "S": "00617"
      }, {
        "S": "00623"
      }, {
        "S": "00627"
      }, {
        "S": "00631"
      }
    ]
  },
  "Locations": {
    "M": {
      "New York": {
        "M": {
          "StatePA": {
            "S": "New York"
          },
          "PostalCodePA": {
            "S": "00501"
          },
          "LocationStatus": {
            "S": "Approved"
          }
        }
      }
    }
  },
  "CognitoReferenceId": {
    "S": "42595544046734671567759365"
  },
  "CompanyEmail": {
    "S": "graniteoffice@dispostable.com"
  },
  "PostalCodePA": {
    "S": "00501"
  },
  "CompanyLogo": {
    "S": "6af8307c2460f2d208ad254f04be4b0d/hUnUG4bmQHegriN7Ke1s26Rz.png"
  },
  "RenewalDate": {
    "N": "1572480000"
  },
  "PGCompanyLogoDocumentId": {
    "S": "0225a429-542b-43bc-8be3-fcec6cb76ee1"
  },
  "LastProspectContractorsNotified": {
    "N": "1567759364"
  },
  "FirstLoginAttempted": {
    "N": "1"
  },
  "StreetLine2MA": {
    "S": "3"
  },
  "LastName": {
    "S": "LT_Kell"
  },
  "CompanyMobileNumber": {
    "S": "121-324-2425"
  },
  "NumberofEmployees": {
    "N": "10"
  },
  "ContractorsCount": {
    "N": "1"
  },
  "EmailVerificationExpires": {
    "S": "8/7/2019"
  },
  "PostalCodeMA": {
    "S": "00501"
  },
  "ContractorId": {
    "S": "us-west-2:6636ea04-1716-483a-9767-01974f91c068"
  },
  "StatePA_Abbr": {
    "S": "NY"
  },
  "DeviceTokens": {
    "L": [{
        "M": {
          "platform": {
            "S": "Web"
          },
          "deviceToken": {
            "S": "dK-AITkBNZI:APA91bHYZmW5Krh98tIoPiPHwmfdiH05wmz51z3LboC9w06s4H9-C68ka9k_nxtpOruHwWAOPKAQtyg43ql69BtUFJqz8mPCs_Tn_kzu_0fPsElNOCoYGzndgisA8ySDqr01BZHRIKxz"
          }
        }
      }
    ]
  },
  "ProfileImage": {
    "S": "2305da72dcb55ee8c8155297a68a8772/ede6febfd4f22e9d3c078c1fe8dbc8e2.png"
  },
  "ContractorType": {
    "S": "Admin"
  },
  "StreetLine1PA": {
    "S": "45-D West Down"
  },
  "Password": {
    "S": "f925916e2754e5e03f75dd58a5733251"
  },
  "TypeOfCompany": {
    "S": "Limited Liability Company LLC"
  },
  "StateMA_Abbr": {
    "S": "NY"
  },
  "StateMA": {
    "S": "New York"
  },
  "Submitted": {
    "N": "0"
  },
  "WorkExperience": {
    "N": "19"
  },
  "ContractorStatus": {
    "N": "1"
  },
  "ProfileGorillaId": {
    "S": "test_data"
  },
  "FirstName": {
    "S": "Test"
  },
  "CityPA": {
    "S": "Hostville"
  },
  "IsCurrentlyWorkingOnClaim": {
    "N": "0"
  },
  "LastWorkedOnClaimTimestamp": {
    "N": "1567759365"
  },
  "MobileNumber": {
    "S": "+11234567890"
  },
  "EmailVerificationCode": {
    "S": "2940434392293323"
  },
  "StreetLine1MA": {
    "S": "3/D Blue yard"
  },
  "ProfileGorillaUserId": {
    "S": "LT_updt4"
  },
  "StatusUpdateDate": {
    "N": "1567159364"
  }
};

let unwanted = ['DeviceTokens']

let changes = Object.keys(jsonDiff.diff(a1,a2));
console.log('changes', changes);

changes = changes.filter(x => !unwanted.includes(x));

console.log(JSON.stringify(changes));
