console.log('starting note.js');
const fs = require('fs');

// module.exports.age = 25;
// module.exports.addNote = (a, b) => {
//     console.log('addNote');
//     return a + b;
// }

let fetchNotes = () => {
    try{
        let notesString = fs.readFileSync('notes-data.json');
        return JSON.parse(notesString);
      }catch(e){
        console.log(e);
        return [];
      }
}

let saveNotes = (notes) => {
    fs.writeFileSync('notes-data.json', JSON.stringify(notes));
}
let removeNote = (title) => {
    let allnotes= fetchNotes();
    let notesAfterRemoval = allnotes.filter((note) => note.title !== title);
    saveNotes(notesAfterRemoval);
}

let addNote = (title, body) => {
  let notes = fetchNotes();
  let note = {
      title,
      body
  }
 
  let duplicateNotes = notes.filter((note) => note.title === title);
  if(duplicateNotes.length === 0){
    notes.push(note);
    saveNotes(notes);
    return note;
  }
}



let getAll = () => {
    console.log('getting all notes');
}
module.exports = {
    addNote,
    getAll,
    removeNote
}